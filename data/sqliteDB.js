const { Database } = require("sqlite3").verbose();
const path = require("path");
const db = new Database(path.join(__dirname, "./", "clients.db"));
const queries = {
  tableClients: `
    CREATE TABLE IF NOT EXISTS clients (
        nitcedula TEXT,
        nombreestablecimiento TEXT,
        nombrecliente TEXT,
        direccion TEXT,
        latitud TEXT,
        longitud TEXT,
        barrio TEXT,
        ciudad TEXT,
        tipodepago TEXT,
        cupodisponible TEXT,
        limitedecredito TEXT,
        grupodelcliente TEXT,
        condicionesdepago TEXT,
        ruta TEXT,
        nitcedula_1 TEXT,
        codigocliente TEXT,
        ruta1 TEXT,
        semanas TEXT,
        diadelasemana TEXT,
        inicioprogramado TEXT,
        duracion TEXT,
        activo TEXT
    );
    `,
};

module.exports = {
  setupDB: () => {
    return new Promise((resolve, reject) => {
      db.serialize(() => {
        db.run(queries.tableClients);
      });
      resolve(true);
    });
  },

  listClient: (nit) => {
    const select =
      "nombreestablecimiento, nombrecliente, direccion, barrio ,ciudad, tipodepago, cupodisponible,limitedecredito, grupodelcliente,condicionesdepago";
    return new Promise((resolve, reject) => {
      let stmt = db.prepare(`SELECT ${select} FROM clients WHERE nitcedula= ?`);
      stmt.get(`${nit}`, (err, row) => {
        if (err) return reject(err);
        resolve(row);
        stmt.finalize();
      });
    });
  },

  createClient: ({
    nitcedula,
    nombreestablecimiento,
    nombrecliente,
    direccion,
    latitud,
    longitud,
    barrio,
    ciudad,
    tipodepago,
    cupodisponible,
    limitedecredito,
    grupodelcliente,
    condicionesdepago,
    ruta,
    nitcedula_1,
    codigocliente,
    ruta1,
    semanas,
    diadelasemana,
    inicioprogramado,
    duracion,
    activo,
  }) => {
    // console.log(nitcedula);
    return new Promise((resolve, reject) => {
      let stmt = db.prepare(
        `INSERT INTO clients VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`
      );
      stmt.run(
        nitcedula,
        nombreestablecimiento,
        nombrecliente,
        direccion,
        latitud,
        longitud,
        barrio,
        ciudad,
        tipodepago,
        cupodisponible,
        limitedecredito,
        grupodelcliente,
        condicionesdepago,
        ruta,
        nitcedula_1,
        codigocliente,
        ruta1,
        semanas,
        diadelasemana,
        inicioprogramado,
        duracion,
        activo
      );
      stmt.finalize((err) => {
        if (err) return reject(err);
        resolve();
      });
    });
  },
  truncateTable: () => {
    return new Promise((resolve, reject) => {
      let stmt = db.prepare("DELETE FROM clients;");
      stmt.run();
      stmt.finalize((err) => {
        if (err) return reject(err);
        resolve();
      });
    });
  },
};
