const { extractArgs } = require("@jjyepez/appargs");
// --- Se extraen los argumentos de la llamada al CLI
const MAPPING = "MAPPING";
const PATH_CSV = "PATH_CSV";
const PATH_DB_CONFIG = "PATH_DB";
const PATH_BACKUP_DB_CONFIG = "PATH_BACKUP_DB";
appArgs = extractArgs(["output", "config", "truncateDB"]);
const {
  readFileConfig,
  validateHeaderCsv,
  appendLogs,
  readFileCsv,
} = require("./utils/fileReader");

const { setupDB } = require("./data/sqliteDB");
const { updateTableClients } = require("./services/clietns");

appendLogs(
  `[Argumentos recibidos]: ${JSON.stringify(appArgs)}`,
  appArgs.output
);

//VALORES DEL ARCHIVO .conf
const config = appArgs.config;
const logPath = appArgs.output;
const truncateDB = appArgs.truncateDB;
const mapping = readFileConfig(config, MAPPING, "json");
const CSV_PATH = readFileConfig(config, PATH_CSV);
const PATH_DB = readFileConfig(config, PATH_DB_CONFIG);
const PATH_BACKUP_DB = readFileConfig(config, PATH_BACKUP_DB_CONFIG);
appendLogs(
  `[Cabecera que se espera del CSV]: ${Object.keys(mapping)}`,
  logPath
);

validateHeaderCsv(CSV_PATH, Object.keys(mapping), logPath); //Validamos la cabecera

const clients = readFileCsv(CSV_PATH, logPath); // Manejamos y formateamos el csv

setupDB();

updateTableClients(clients, PATH_DB, PATH_BACKUP_DB, logPath, truncateDB).then(
  (result) => {
    const msg = `Total de registros insertados [${result}]`;
    console.log("Completado!");
    appendLogs(msg, logPath);
  }
);

