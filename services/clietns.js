const fs = require("fs");
const date = new Date();
const dateNow = `${
  date.getMonth() + 1
}-${date.getDate()}-${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`;

const { listClient, createClient, truncateTable } = require("../data/sqliteDB");
const { appendLogs } = require("../utils/fileReader");
async function updateTableClients(
  clients,
  dbPath,
  dbBackUpPath,
  logsPath,
  truncateDB
) {
  try {
    let count = [];
    appendLogs("Creando backup de la Base de Datos", logsPath);
    //creamos el backup
    fs.createReadStream(`${dbPath}`) //Copiamos el archivo db
      .on("error", (err) => {
        appendLogs(`[ERROR]  ${err}`, logsPath);
      })
      .pipe(fs.createWriteStream(`${dbBackUpPath}-${dateNow}.db`)) // Lo pegamos en la nueva ruta
      .on("error", (err) => {
        appendLogs(`[ERROR]  ${err}`, logsPath);
        return false;
      });
    if (truncateDB == "true") {
      await truncateTable();
      appendLogs(`[DB] clients TRUNCADA`, logsPath);
    }

    await Promise.all(
      clients.map(async (client, i) => {
        try {
          const clientExist = await listClient(client.nitcedula);
          if (!clientExist) {
            appendLogs(
              `Creando cliente Nit/cedula: ${client.nitcedula}`,
              logsPath
            );
            await createClient(client);
            console.log(`Creando cliente Nit/cedula: ${client.nitcedula}`);

            count.push(i); //contamos cuantos clientes se estan insertando
          }
          return false;
        } catch (error) {
          next(error);
        }
      })
    );

    return count.length;
  } catch (error) {
    appendLogs(`[ERROR]  ${error}`, logsPath);
  }
}
module.exports = {
  updateTableClients,
};
