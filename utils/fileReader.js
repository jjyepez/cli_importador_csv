const fs = require("fs");
const CSVSEPARATOR = ";";
/**
 * @param {String} value
 * Arregla el string que viene del csv que ya viene entre comillas
 */
const formatToString = (value) => {
  const result = value.trim().replace(/['"]+/g, "");
  return result;
};

/**
 * @param {String} path ruta del archivo
 */
function readFile(path) {
  const content = fs.readFileSync(path, "utf8");

  if (content) {
    return content;
  }
  return false;
}

/**
 * @param {String} path ruta del archivo
 * @param {String} keyword keyword de la que queremos obtener el valor
 * @param {String} format formato en que se va a devolver json || string
 */
function readFileConfig(path, keyword, format) {
  let result = false;
  const content = readFile(path);
  const arrayContent = content.split("\n");
  arrayContent.forEach((element, i) => {
    const _keyword = element.split("=")[0];
    //comparamos el key enviado con los del array
    if (_keyword == keyword) {
      //verificamos el formato de retorno
      if (format == "json") {
        return (result = JSON.parse(element.split("=")[1]));
      } else {
        return (result = element.split("=")[1]);
      }
    }
  });

  return result;
}

/**
 * @param {Array} headerFieldSstructure Estructura correcta del header
 * @param {Array} headerFildsCsv Campos del header que llegan desde el CSV
 */
const compareHeaderCsv = (headerFieldSstructure, headerFildsCsv, logPath) => {
  let errors = [];
  const result = headerFieldSstructure.map((headerKeyword, i) => {
    const formatString = formatToString(headerFildsCsv[i]);
    if (headerKeyword == formatString) {
      return true;
    } else {
      appendLogs(
        `[ERROR] El campo ${formatString} de la cabecera del csv no coinciden con el de la estructura`,
        logPath,
        true
      );
    }
  });
};

function validateHeaderCsv(pathCsv, validHeader, logPath) {
  const content = readFile(pathCsv);
  const arrayContent = content.split("\n");
  const header = arrayContent[0].split(CSVSEPARATOR);

  //Validamos el header
  compareHeaderCsv(validHeader, header, logPath);
}
/**
 * @param {String} path ruta del archivo
 * @param {Array} validateHeader header válido
 */
function readFileCsv(csvPath, logsPath) {
  appendLogs("Leyendo el archivo CSV...", logsPath);
  const content = readFile(csvPath);
  const arrayContent = content.split("\n");

  delete arrayContent[0]; //una vez validado eliminamos el header del csv, ya que no lo necesitamos

  let clients = [];

  appendLogs("Procesando el archivo CSV...", logsPath);
  //   [arrayContent]  este es un único array que contiene toda la data en un string
  for (let index = 1; index < arrayContent.length; index++) {
    let element = arrayContent[index];
    element = element.split(CSVSEPARATOR); //pasamos de string a Array
    // validamos que el array tenga mas de 2 valores al menos para evitar arrays vacios
    if (element.length > 1) {
      if (element[0] == "") {
        return appendLogs(
          `[ERROR] El campo nit/cédula no puede estar vacío - [fila ${
            index + 1
          }]`,
          logsPath,
          true
        );
      }
      clients.push(element);
    }
  }

  const clientsResult = clients.map((element, i) => ({
    nitcedula: formatToString(element[0]),
    nombreestablecimiento: formatToString(element[1]),
    nombrecliente: formatToString(element[2]),
    direccion: formatToString(element[3]),
    latitud: formatToString(element[4]),
    longitud: formatToString(element[5]),
    barrio: formatToString(element[6]),
    ciudad: formatToString(element[7]),
    tipodepago: formatToString(element[8]),
    cupodisponible: formatToString(element[9]),
    limitedecredito: formatToString(element[10]),
    grupodelcliente: formatToString(element[11]),
    condicionesdepago: formatToString(element[12]),
    ruta: formatToString(element[13]),
    nitcedula_1: formatToString(element[14]),
    codigocliente: formatToString(element[15]),
    ruta1: formatToString(element[16]),
    semanas: formatToString(element[17]),
    diadelasemana: formatToString(element[18]),
    inicioprogramado: formatToString(element[19]),
    duracion: formatToString(element[20]),
    activo: formatToString(element[21]),
  }));
  appendLogs("Procesado con éxito...", logsPath);
  return clientsResult;
}

/**
 * @param {String} error Error que se va a mostrar en el archivo
 * @param {String} path Ruta del archivo en donde se escribira el error
 */
function appendLogs(message, path, processCancel) {
  const date = new Date();
  const dateLog = `${
    date.getMonth() + 1
  }-${date.getDate()}-${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`;

  const msg = `"${dateLog}" - ${message} \n`;
  fs.appendFile(path, msg, (err) => {
    console.log(msg);
    if (processCancel) {
      process.exit(1);
    }
    if (err) {
      console.log(err);
      process.exit(1);
    }
  });
}

module.exports = {
  readFileConfig,
  readFileCsv,
  validateHeaderCsv,
  appendLogs,
};
